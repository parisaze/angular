import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  colorcode:number;


  constructor() {

    this.colorcode= Math.random();


  }

  ngOnInit(): void {
  }

  getcolor(){
    return this.colorcode > 0.5 ? 'red' : 'green';

  }
}