import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms' ;
import { AppComponent } from './app.component';
import  { TestComponent } from './test.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NewComponent } from './new/new.component';

@NgModule({
  declarations: [
    AppComponent,
      TestComponent,
      NewComponent
  ],
  imports: [
    BrowserModule ,
      FormsModule,
      NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})

